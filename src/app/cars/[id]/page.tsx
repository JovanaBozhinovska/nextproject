import { Car } from "@/app/page";
import AutoCard from "@/components/AutoCard";

async function fetchCar(id: string): Promise<Car> {
  const car = await fetch(`http://localhost:5001/cars/${id}`, {
    cache: "no-store",
  });

  return car.json();
}

export default async function CarDetails({
  params,
}: {
  params: { id: string };
}) {
  const car = await fetchCar(params.id);
  return (
    <div className="container">
      <div className="row">
        <div className="col-2">
          <AutoCard {...car} />
        </div>
      </div>
    </div>
  );
}
