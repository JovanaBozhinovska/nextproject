import Image from "next/image";

interface About {
  title: string;
  first_paragraph: string;
  second_paragraph: string;
}

async function fetchAbout(): Promise<About> {
  const data = await fetch("http://localhost:5001/about_page", {
    cache: "no-store",
  });
  return data.json();
}

export default async function About() {
  const aboutData = await fetchAbout();
  return (
    <>
      <Image
        src="https://www.lamborghini.com/sites/it-en/files/DAM/lamborghini/facelift_2019/models_gw/hero-banner/2022/04_12/gate_models_og_01.jpg"
        alt=""
        width={900}
        height={500}
        className="w-100 small-banner"
      />

      <div className="container mt-5">
        <div className="row">
          <div className="col">
            <h1>{aboutData.title}</h1>
            <p className="lead">{aboutData.first_paragraph}</p>
            <p>{aboutData.second_paragraph}</p>
          </div>
        </div>
      </div>
    </>
  );
}
