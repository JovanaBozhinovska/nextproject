import AutoCard from "@/components/AutoCard";
import Banner from "@/components/Banner";
import Filters from "@/components/Filters";
import Link from "next/link";

export interface Car {
  id: number;
  name: string;
  slug: string;
  price: number;
  image: string;
  horsepower: number;
  weight: number;
  acceleration: number;
  year: number;
  origin: string;
}

// async function fetchData(): Promise<{ cars: Car[] }> {
//   const res = await fetch("http://localhost:5001/cars");
//   const cars = await res.json();

//   return { cars };
// }

async function fetchData(origin?: string, year?: string): Promise<Car[]> {
  let url = "http://localhost:5001/cars?";
  if (origin) url += `origin=${origin}`;
  if (year) url += `${origin ? "&" : ""}_sort=${year === "asc" ? "" : "-"}year`;

  console.log(url);
  const res = await fetch(url, { cache: "no-store" });

  return res.json();
}

export default async function Home({
  searchParams,
}: {
  searchParams: { origin: string; year: string };
}) {
  const cars = await fetchData(searchParams.origin, searchParams.year);
  return (
    <>
      <Banner />
      <Filters />

      <div className="container">
        <div className="row">
          {cars.map((car) => {
            return (
              <Link
                key={car.id}
                href={`cars/${car.id}`}
                className="col-2 mb-4"
                style={{ textDecoration: "none" }}
              >
                <AutoCard {...car} />
              </Link>
            );
          })}
        </div>
        ;
      </div>
    </>
  );
}
