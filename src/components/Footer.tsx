import React from "react";

const Footer: React.FC = () => {
  return (
    <div className="footer bg-dark text-white py-3 text-center mt-5 position-sticky">
      <p className="mb-0">Copyright &copy; 2022 Brainster</p>
    </div>
  );
};

export default Footer;
