"use client";

import { useRouter, useSearchParams } from "next/navigation";
import React, { useEffect, useState } from "react";

const originFilters = [
  {
    value: "USA",
  },
  {
    value: "France",
  },
  {
    value: "Japan",
  },
  {
    value: "United Kingdom",
  },
  {
    value: "Germany",
  },
];

const Filters = () => {
  const query = useSearchParams();
  const [origin, setOrigin] = useState<string>(query.get("origin") || "");
  const [year, setYear] = useState<string>(query.get("year") || "");
  const router = useRouter();

  // ?origin=USA&year=desc
  useEffect(() => {
    let query = "?";
    if (origin) {
      query += `origin=${origin}`;
    }
    if (year) {
      query += `${origin && "&"}year=${year}`;
    }
    router.push(query);
  }, [origin, year]);

  return (
    <div className="filters container bg-dark text-white rounded -mt-5 mb-5">
      <div className="row align-items-center">
        <div className="px-5 py-4 w-100">
          <form className="row">
            {/* FILTER BY ORIGIN */}
            <div className="col">
              <label htmlFor="origin">Filter by origin:</label>
              <select
                className="form-control"
                id="origin"
                value={origin}
                onChange={(e) => {
                  setOrigin(e.target.value);
                }}
              >
                <option disabled value={""}>
                  Filter by origin:
                </option>
                {originFilters.map((f, i) => (
                  <option key={`origin-filter-${i}`} value={f.value}>
                    {f.value}
                  </option>
                ))}
              </select>
            </div>

            {/* FILTER BY YEAR */}
            <div className="col">
              <label htmlFor="year">Sort by year:</label>

              <select
                className="form-control"
                id="year"
                value={year}
                onChange={(e) => {
                  setYear(e.target.value);
                }}
              >
                <option disabled value={""}>
                  Filter by year:
                </option>
                <option value="asc">Oldest First</option>
                <option value="desc">Newest First</option>
              </select>
            </div>

            {/* show the remove filters button only if there are filters */}
            {!!(origin || year) && (
              <div className="col d-flex flex-column">
                <button
                  type="button"
                  className="btn btn-danger btn-block mt-auto"
                  onClick={() => {
                    setOrigin("");
                    setYear("");
                  }}
                >
                  Remove filters
                </button>
              </div>
            )}
          </form>
        </div>
      </div>
    </div>
  );
};

export default Filters;
