import { Car } from "@/app/page";
import React from "react";

const AutoCard = ({ name, origin, year, image }: Car) => {
  return (
    <div>
      <div className="card h-100 text-dark">
        <div className="card-body h-150 d-flex align-items-center">
          <img className="img-fluid" src={image} alt="Card image cap" />
        </div>
        <div className="card-body px-3">
          <h2 className="h5 card-title">{name}</h2>
          <p className="mb-0">
            <small className="card-text">{origin}</small>
          </p>
          <p className="mb-0">
            <small className="card-text">{year}</small>
          </p>
        </div>
      </div>
    </div>
  );
};

export default AutoCard;
