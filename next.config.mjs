/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'www.lamborghini.com',
            },
        ],
    },
};

export default nextConfig;
